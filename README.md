# Vue 3, vue-router, ts, scss and webpack base project
Base project setup for Vue3 with Vue-router using typescript, webpack and webpack dev-server.

Base project including:
- Vue 3 
- Vue-router 
- Webpack 
- Webpack dev-server 
- Typescript SCSS

To run the example you have to:
- intall dependencies with `npm install`
- run webpack with `webpack-dev-server`
- visit `http://localhost:8080/`
